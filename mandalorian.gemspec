# frozen_string_literal: true

$LOAD_PATH.push File.expand_path('lib', __dir__)

require 'mandalorian/version'

Gem::Specification.new do |spec|
  spec.name        = 'mandalorian'
  spec.version     = Mandalorian::VERSION
  spec.authors     = ['Artis Raugulis']
  spec.email       = ['artis@devart.lv']
  spec.homepage    = 'https://github.com/devart-lv/mandalorian'
  spec.summary     = ''
  spec.description = ''
  spec.license     = 'MIT'

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise 'RubyGems 2.0 or newer is required to protect against ' \
      'public gem pushes.'
  end

  spec.files = Dir['{app,config,db,lib}/**/*', 'MIT-LICENSE', 'Rakefile', 'README.md']

  spec.add_runtime_dependency 'cocoon'
  spec.add_runtime_dependency 'elastic-apm'
  spec.add_runtime_dependency 'haml'
  spec.add_runtime_dependency 'rails'
  # spec.add_dependency 'remotipart'

  spec.add_development_dependency 'bundler', '~> 2.0'
  spec.add_development_dependency 'rake', '~> 13.0'
  spec.add_development_dependency 'rubocop'
  spec.add_development_dependency 'rspec'
  spec.add_development_dependency 'rspec-rails'
  spec.add_development_dependency 'pg'
end
