require 'spec_helper'

describe Mandalorian::ActionView::Helpers::ImageHelper, type: :helper do

  context 'when test environment' do
    before do
      allow(Rails).to receive(:env).and_return(OpenStruct.new(test?: true))
    end

    describe 'when edit translations is not enabled' do
      context 'with jpeg' do
        let(:output) {
          helper.mandalorian_image_tag 'devart.jpeg'
        }

        it 'outputs an image tag directly' do
          expect(output.match?(/<img src=\"\/assets\/devart-.{64}.jpg\" \/>/)).to be(true)
        end
      end

      context 'with svg' do
        let(:output) {
          helper.mandalorian_image_tag 'devart.svg'
        }

        it 'outputs an svg directly' do
          expect(output.match?(/<img src=\"\/assets\/devart-.{64}.svg\" \/>/)).to be(true)
        end
      end
    end

    describe 'when edit translations is enabled' do
      before do
        @request.session['edit_translations'] = true
      end
      
      let(:output) {
        helper.mandalorian_image_tag 'devart.jpeg'
      }

      it 'outputs an image tag directly instead of mandalorian tag' do
        expect(output.match?(/<img src=\"\/assets\/devart-.{64}.jpg\" \/>/)).to be(true)
      end
    end

  end

  describe 'production environemt' do
    before do
      allow(Rails).to receive(:env).and_return(OpenStruct.new(test?: false))
    end

    describe 'when edit translations is not enabled' do
      context 'with jpeg' do
        let(:output) {
          helper.mandalorian_image_tag 'devart.jpeg'
        }

        it 'outputs an image tag' do
          expect(output.match?(/<img src="http\:\/\/test.host\/rails\/active_storage\/blobs\/.{80}--.{40}\/devart.jpeg" \/>/)).to be(true)
        end
      end

      context 'with svg' do
        let(:output) {
          helper.mandalorian_image_tag 'devart.svg'
        }

        it 'outputs an svg' do
          expect(output.match?(/<svg.+?<\/svg>/)).to be(true)
        end
      end
    end

    describe 'when edit translations is enabled' do
      before do
        @request.session['edit_translations'] = true
      end
      
      let(:output) {
        helper.mandalorian_image_tag 'devart.jpeg'
      }

      it 'outputs a mandalorian component' do
        expect(output.match?(/<mandalorian.+?<\/mandalorian>/)).to be(true)
      end
    end
  end
end