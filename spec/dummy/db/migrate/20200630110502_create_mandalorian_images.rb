# frozen_string_literal: true

class CreateMandalorianImages < ActiveRecord::Migration[6.0]
  def change
    create_table :mandalorian_images do |t|
      t.string :locale
      t.string :key

      t.timestamps
    end

    add_index :mandalorian_images, :locale
    add_index :mandalorian_images, :key
  end
end
