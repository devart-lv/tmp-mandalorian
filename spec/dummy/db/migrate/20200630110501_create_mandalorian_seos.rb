# frozen_string_literal: true

class CreateMandalorianSeos < ActiveRecord::Migration[6.0]
  def change
    create_table :mandalorian_seos do |t|
      t.string :locale
      t.string :key
      t.string :title
      t.text :keywords
      t.text :description

      t.timestamps
    end

    add_index :mandalorian_seos, :locale
    add_index :mandalorian_seos, :key
  end
end
