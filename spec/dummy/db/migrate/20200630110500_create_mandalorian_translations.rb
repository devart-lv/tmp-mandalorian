# frozen_string_literal: true

class CreateMandalorianTranslations < ActiveRecord::Migration[6.0]
  def change
    create_table :mandalorian_translations do |t|
      t.string :locale
      t.string :key
      t.text   :value

      t.timestamps
    end

    add_index :mandalorian_translations, :locale
    add_index :mandalorian_translations, :key
  end
end
