// Load all the lib js within this directory and all subdirectories.
// Lib files must be named *.js.

const libs = require.context('.', true, /\.js$/)
libs.keys().forEach(libs)