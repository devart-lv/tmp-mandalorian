// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.

require("@rails/ujs").start()
require("@rails/activestorage").start()
require('jquery')
// require('jquery-ui-sortable')
// require('jquery.cookie')

import $ from 'jquery';
window.jQuery = $;
window.$ = $;

require("channels")
require('vendor')
require('lib')
require('controllers')

import 'bootstrap/dist/js/bootstrap';
import 'cocoon-js';

$(document).ready(function () {
  mandalorian.init();
});
