// Load all the controller js within this directory and all subdirectories.
// Controller files must be named *.js.

const controller = require.context('.', true, /\.js$/)
controller.keys().forEach(controller)