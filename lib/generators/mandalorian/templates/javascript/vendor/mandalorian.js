/* DO NOT EDIT THIS FILE DIRECTLY */
mandalorian = {
  onclick: function () {
    $("[data-onclick]").on('click', function(e) {
      e.preventDefault();
      window.location = $(this).data('onclick');
    });
  },

  edit: function() {
    $('.mandalorian').on('click', function (e) {
      e.preventDefault();

      if($(this).data('mandaloriankey') == undefined) {
        lookup = $(this).attr('href');
      } else {
        lookup = '/mandalorian/translations/lookup?key='+$(this).data('mandaloriankey');
      }

      $.ajax({
        url: lookup,
        dataType: "script",
        success: function (result) {
          eval(result);
        }
      });
    });
  },

  enable_translations: function() {
    if(typeof translations_enabled !== 'undefined' && translations_enabled == true) {
      $('[data-mandaloriankey]').addClass('mandalorian');
    }
  },

  drag: function() {
    if(document.getElementById('mandalorian') == null){
      return false;
    }
    elmnt = document.getElementById("mandalorian");

    dragElement(document.getElementById("mandalorian"));

    function dragElement(elmnt) {
      var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
      if (document.getElementById(elmnt.id + "header")) {
        document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
      } else {
        elmnt.onmousedown = dragMouseDown;
      }
    }

    function dragMouseDown(e) {
      e = e || window.event;
      e.preventDefault();
      pos3 = e.clientX;
      pos4 = e.clientY;
      document.onmouseup = closeDragElement;
      document.onmousemove = elementDrag;
    }

    function elementDrag(e) {
      e = e || window.event;
      e.preventDefault();
      pos1 = pos3 - e.clientX;
      pos2 = pos4 - e.clientY;
      pos3 = e.clientX;
      pos4 = e.clientY;
      elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
    }

    function closeDragElement() {
      document.onmouseup = null;
      document.onmousemove = null;
    }
  },

  is_touch_device: function() {
    var prefixes = ' -webkit- -moz- -o- -ms- '.split(' ');
    var mq = function(query) {
      return window.matchMedia(query).matches;
    }
    
    if (('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch) {
      return true;
    }
    
    // include the 'heartz' as a way to have a non matching MQ to help terminate the join
    // https://git.io/vznFH
    var query = ['(', prefixes.join('touch-enabled),('), 'heartz', ')'].join('');

    return mq(query);
  },

  exec: function( namespace, controller, action ){
    ns_controller = String('ns_'+namespace+ '_' + controller);

    var ns = window[ns_controller];

    if(ns == undefined){
      if (window.console) { console.log('NOTICE: namespace doesnt exist! ' + ns_controller + ' with action ' + action); }
      return true
    }

    var action = ( action === undefined ) ? "init" : action;

    if(controller !== "" && ns[controller] && typeof ns[controller][action] == "function"){
      ns[controller][action]();
    }
  },

  init: function () {
    mandalorian.enable_translations();
    mandalorian.onclick();
    mandalorian.edit();
    mandalorian.drag();

    var body       = document.body;
    var controller = body.getAttribute( "data-controller" );
    var action     = body.getAttribute( "data-action" );
    var namespace  = body.getAttribute( "data-name-space" );

    if(typeof translations_enabled == 'undefined' || translations_enabled != true) {
      mandalorian.exec( 'global', "common" );
      mandalorian.exec( namespace, controller );
      mandalorian.exec( namespace, controller, action );
    }
  }
}