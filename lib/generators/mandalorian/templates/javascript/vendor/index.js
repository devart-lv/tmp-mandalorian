// Load all the vendor js within this directory and all subdirectories.
// Vendor files must be named *.js.

const vendor = require.context('.', true, /\.js$/)
vendor.keys().forEach(vendor)