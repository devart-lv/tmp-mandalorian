# frozen_string_literal: true

require 'rails/generators'
require 'rails/generators/migration'
require 'rails/generators/active_record'

module Mandalorian
  module Generators
    class InstallGenerator < Rails::Generators::Base
      include Rails::Generators::Migration

      def self.next_migration_number(path)
        @prev_migration_nr ||= ActiveRecord::Generators::Base.next_migration_number(path).to_i - 1
        @prev_migration_nr += 1

        @prev_migration_nr.to_s
      end

      source_root File.expand_path('templates', __dir__)

      def install_mandalorian
        move_files
        configure_application
        add_mount_point
        add_gems
        rake 'db:migrate'
        yarn_setup
        overcommit_setup
      end

      # def install_views
      #   copy_files 'views', 'app/views'
      # end

      # def install_javascripts_assets
      #   copy_files 'assets/javascripts', 'lib/assets/javascripts'
      # end

      # def install_stylesheets_assets
      #   copy_files 'assets/stylesheets', 'lib/assets/stylesheets'
      # end

      # def install_controllers
      #   copy_files 'controllers', 'app/controllers'
      # end

      private

        def move_files
          copy_files 'initializers', 'config/initializers'
          copy_files 'config', 'config'
          copy_files 'javascript/controllers', 'app/javascript/controllers'
          copy_files 'javascript/lib', 'app/javascript/lib'
          copy_files 'javascript/packs', 'app/javascript/packs'
          copy_files 'javascript/vendor', 'app/javascript/vendor'
          copy_files 'views/application', 'app/views/application'
          copy_files 'views/layouts', 'app/views/layouts'
          copy_files 'services', 'app/services'
          copy_files 'presenters', 'app/presenters'
          copy_files 'helpers', 'app/helpers'
          install_migrations
          copy_files 'base', './'
        end

        def install_migrations
          get_file_list('migrations').each do |migration|
            migration_template "migrations/#{migration}", "db/migrate/#{migration}"
          end
        end

        def configure_application
          app_config = +"    config.i18n.available_locales = %i[en ru lv]\n"
          app_config << "    config.hosts << 'lvh.me'\n"

          inject_into_file 'config/application.rb', after: "# the framework and any gems in your application.\n" do
            app_config
          end
        end

        def add_mount_point
          mount_point = +"mount_mandalorian_at 'root' do\n"
          mount_point << "end\n"
          route mount_point
        end

        def add_gems
          gem 'haml'
          gem 'elastic-apm'

          gem_group :development, :test do
            gem 'rubocop'
            gem 'rubocop-rspec'
            gem 'overcommit'
            gem 'rspec-rails'
          end

          Bundler.with_clean_env do
            run 'bundle update'
          end
        end

        def yarn_setup
          run 'yarn add stylelint'
          run 'yarn add stylelint-config-standard'
          run 'yarn add bootstrap'
          run 'yarn add cocoon-js'
          run 'yarn add popper.js'
        end

        def overcommit_setup
          run 'overcommit --install'
          run 'overcommit --sign'
        end

        def copy_files(subdir, dest_dir)
          raise ArgumEnterror unless subdir.is_a? String
          raise ArgumEnterror unless dest_dir.is_a? String
          raise ArgumetnError if subdir.blank?
          raise ArgumetnError if dest_dir.blank?

          get_file_list(subdir).each do |image|
            copy_file [subdir, image].join('/'), [dest_dir, image].join('/')
          end
        end

        def get_file_list(subdir)
          raise ArgumentError unless subdir.is_a? String
          raise ArgumetnError if subdir.blank?

          dir = get_current_dir
          search_path = [dir, 'templates', subdir].join('/') + '/'
          directory_listing = Dir.glob(search_path + '**/*', File::FNM_DOTMATCH)
          file_list = directory_listing.map { |filename| File.directory?(filename) ? nil : filename.sub(search_path, '') }
          file_list.delete nil
          file_list.delete '.'
          file_list.delete '..'
          file_list
        end

        def get_current_dir
          File.dirname(__FILE__)
        end
    end
  end
end
