# frozen_string_literal: true

module Mandalorian
  module ActionDispatch
    module Routing
      module Mapper
        # Pass given resource to "resources" mount method and
        # add extra routes for members and collections
        def mandalorian_resources(*args)
          resources(*args) do
            yield if block_given?
          end
        end

        # Mounts mandalorian at <tt>mount_location</tt> location.
        # e.g.
        #   mount_mandalorian_at "root" do
        #     mandalorian_resources :posts
        #   end
        #
        # rubocop:disable Metrics/MethodLength
        def mount_mandalorian_at(mount_location, _options = {})
          scope mount_location do
            namespace :mandalorian, path: nil do
              mandalorian_resources :seos
              mandalorian_resources :images
              mandalorian_resources :translations do
                collection do
                  get 'lookup'
                  get 'enable'
                  get 'disable'
                end
              end
              mandalorian_resources :sessions, only: %w[new destroy]

              yield if block_given?
            end
          end

          get mount_location => 'mandalorian/sessions#new', as: :login
        end
        # rubocop:enable Metrics/MethodLength
      end
    end
  end
end
