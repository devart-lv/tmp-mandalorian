# frozen_string_literal: true

require 'mandalorian/action_dispatch/routing/mapper'
require 'mandalorian/action_controller/acts/restful'
require 'mandalorian/action_view/helpers/seo_helper'
require 'mandalorian/action_view/helpers/translation_helper'
require 'mandalorian/action_view/helpers/image_helper'

module Mandalorian
  class Engine < ::Rails::Engine
    ::ActionDispatch::Routing::Mapper.include Mandalorian::ActionDispatch::Routing::Mapper

    ::ActionController::Base.include Mandalorian::ActionController::Acts::Restful

    ::ActionView::Base.include Mandalorian::ActionView::Helpers::SeoHelper
    ::ActionView::Base.include Mandalorian::ActionView::Helpers::TranslationHelper
    ::ActionView::Base.include Mandalorian::ActionView::Helpers::ImageHelper

    config.after_initialize do
      require 'i18n/mandalorian'
    end
  end
end
