# frozen_string_literal: true

module Mandalorian
  module ActionController
    module Acts
      module Restful
        extend ActiveSupport::Concern

        module ClassMethods
          def acts_as_restful(active_record_model, options = {})
            before_action :load_record, only: %i[show edit update destroy]
            before_action :load_items, only: %i[index destroy]

            include InstanceMethodsOnActivation

            cattr_accessor :model do
              active_record_model
            end

            cattr_accessor :permited_params do
              options[:permit_params] if options.include? :permit_params
            end

            cattr_accessor :order_by do
              options[:order_by] if options.include? :order_by
            end

            cattr_accessor :paginate do
              options[:paginate] if options.include? :paginate
            end
          end
        end

        # rubocop:disable Metrics/ModuleLength
        module InstanceMethodsOnActivation
          def index
            yield if block_given?
          end

          def new
            @item = model.new
            load_translations
            yield if block_given?
          end

          def show
            yield if block_given?
          end

          def edit
            load_translations
            yield if block_given?
          end

          def create
            @item = model.new(items_params)

            yield if block_given?

            if @item.save
              create_callbacks
              after_create_response
            else
              render action: :new
            end
          end

          def update
            yield if block_given?

            if @item.update(items_params)
              update_callbacks
              after_update_response
            else
              render action: :new
            end
          end

          def destroy
            yield if block_given?
            @item.destroy
            destroy_callbacks
            after_destroy_response
          end

          def order
            sequence = URI.decode_www_form(params[:sortable]).map { |i| i[1] }
            sequence.each_with_index do |id, index|
              item = model.find_by id: id
              item.sequence = index
              item.save!
            end
            render plain: 200
          end

          protected

            def items_params
              @items_params ||= params.require(parameterized_name).permit(permited_params)
            end

            def collection
              @collection ||= begin
                return model.filter_by params if model.respond_to? :filter_by
              end
            end

            def parameterized_name
              model.model_name.to_s.underscore.downcase
            end

          private

            def after_create_response
              respond_to do |format|
                format.html { after_create_action }
                format.js   { after_create_js_action }
              end
            end

            def after_update_response
              respond_to do |format|
                format.html { after_update_action }
                format.js   { after_update_js_action }
              end
            end

            def after_destroy_response
              respond_to do |format|
                format.html { after_destroy_action }
                format.js   { after_destroy_js_action }
              end
            end

            def after_create_action
              redirect_to action: :index
            end

            def after_create_js_action
              render action: :success
            end

            def after_update_action
              redirect_to action: :index
            end

            def after_update_js_action
              render action: :success
            end

            def after_destroy_action
              redirect_to action: :index
            end

            def after_destroy_js_action
              render action: :index
            end

            def model
              model
            end

            # rubocop:disable Metrics/AbcSize
            def load_collection
              return @load_collection if @load_collection.present?
              return unless model

              @load_collection = collection || model.all
              @load_collection = @load_collection.order(order_by) if order_by.present?
              @load_collection = @load_collection.page(params[:page]).per(paginate) if paginate.present?
              @load_collection
            end
            # rubocop:enable Metrics/AbcSize

            def load_record
              @item = model.find(params[:id])
            end

            def load_items
              @items = load_collection
            end

            def load_translations
              return unless @item.respond_to? :translations

              I18n.available_locales.each { |locale| @item.translations.find_or_initialize_by(locale: locale) }
            end

            def create_callbacks; end

            def update_callbacks; end

            def destroy_callbacks; end
        end
        # rubocop:enable Metrics/ModuleLength
      end
    end
  end
end
