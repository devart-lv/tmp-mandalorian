# frozen_string_literal: true

module Mandalorian
  module ActionView
    module Helpers
      module ImageHelper
        def mandalorian_image_tag(file, options = {})
          return image_tag file, options if Rails.env.test?

          image = lookup_image(file, options[:name] || file)

          if session[:edit_translations] == true
            "<mandalorian class='mandalorian' data-remote='true' href='/root/images/#{image.id}/edit'"\
              " data-mandalorian-edit='#{options[:name] || file}'>#{output(image, options)}</mandalorian>".html_safe
          else
            output(image, options)
          end
        end

        private

          def lookup_image(file, name)
            @images ||= I18n::Backend::Mandalorian::Image.with_attached_file.where(locale: locale).map do |image|
              [image.key, image]
            end.to_h

            return @images[name] if @images[name].present?

            create_image(file, name)
          end

          def create_image(file, name)
            image = I18n::Backend::Mandalorian::Image.create!(
              locale: locale,
              key: name
            )
            image.file.attach(io: File.open(Rails.root.join('app', 'assets', 'images', file)), filename: file)
            image
          end

          def output(image, options)
            if image.svg?
              image.file.download.to_s.html_safe
            else
              image_tag image.file, options
            end
          end
      end
    end
  end
end
