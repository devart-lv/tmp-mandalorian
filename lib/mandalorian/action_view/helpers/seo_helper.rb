# frozen_string_literal: false

module Mandalorian
  module ActionView
    module Helpers
      module SeoHelper
        def seo_tags
          buffer = "<title>#{seo.title}</title>"
          buffer << "<meta content='#{seo.description}' name='description'>"
          buffer << "<meta content='#{seo.keywords}' name='keywords'>"

          buffer.html_safe
        end

        def seo
          @seo ||= lookup_seo
        end

        def embed
          lookup_seo.embed
        end

        private

          def lookup_seo
            I18n::Backend::Mandalorian::Seo.where(locale: locale).find_or_create_by(key: controller_name + action_name)
          end
      end
    end
  end
end
