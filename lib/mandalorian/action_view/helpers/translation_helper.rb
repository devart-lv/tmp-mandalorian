# frozen_string_literal: true

module Mandalorian
  module ActionView
    module Helpers
      module TranslationHelper
        extend ActiveSupport::Concern

        def translate(key, options = {})
          if options[:drop]
            I18n::Backend::Mandalorian::Translation.where(locale: locale, key: key).destroy_all
            I18n::Backend::Mandalorian::Translation.instance_variable_set('@translations_updated_at', nil)
            raise 'DroppedKey'
          end

          result = super(key, **options)

          embed = options.key?(:embed) ? options[:embed] : true

          session = (session rescue nil)

          if session && session[:edit_translations] == true && embed
            translation = I18n::Backend::Mandalorian::Translation.where(locale: locale).find_by(key: key)
            "<mandalorian class='mandalorian' data-remote='true'"\
              " href='/root/translations/#{translation.id}/edit'"\
              " data-mandalorian-edit='#{key}'>#{result}</mandalorian>".html_safe
          else
            result
          end
        end
        alias t translate
      end
    end
  end
end
