# frozen_string_literal: true

require 'i18n'

I18n.module_eval do
  # Unset a constant without private access.
  def self.const_unset(const)
    instance_eval { remove_const(const) }
  end
end

I18n.const_unset('RESERVED_KEYS')

module I18n
  RESERVED_KEYS = %i[
    cascade
    deep_interpolation
    default
    exception_handler
    fallback
    fallback_in_progress
    format
    object
    raise
    resolve
    scope
    separator
    throw
    translation
    drop
  ].freeze
end
