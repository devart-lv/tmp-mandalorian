# frozen_string_literal: true

require 'i18n/i18n_mandalorian'
require 'i18n/backend/mandalorian'
I18n.backend = I18n::Backend::Chain.new(I18n::Backend::Mandalorian.new, I18n::Backend::Simple.new)
