# frozen_string_literal: true

require 'active_record'

module I18n
  module Backend
    class Mandalorian
      class Seo < ::ActiveRecord::Base
        self.table_name = 'mandalorian_seos'

        validates :key, uniqueness: { scope: :locale }
        validates :key, presence: true

        def model_name
          ActiveModel::Name.new(self, nil, 'MandalorianSeo')
        end
      end
    end
  end
end
