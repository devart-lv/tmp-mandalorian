# frozen_string_literal: true

require 'active_record'

module I18n
  module Backend
    class Mandalorian
      class Translation < ::ActiveRecord::Base
        self.table_name = 'mandalorian_translations'

        validates :key, uniqueness: { scope: :locale }
        validates :key, presence: true

        def model_name
          ActiveModel::Name.new(self, nil, 'MandalorianTranslation')
        end

        @translations = nil
        @translations_updated_at = nil

        class << self
          def lookup(locale, key, _scope = [], _options = EMPTY_HASH)
            reset_translations

            @translations ||= begin
              translations = {}
              I18n.available_locales.each do |available_locale|
                translations[available_locale] = where(locale: available_locale).map do |translation|
                  [translation.key, translation.value]
                end.to_h
              end
              translations
            end

            @translations[locale.to_sym][key]
            # find_by(locale: locale, key: key)&.value
          end

          def store(locale:, key:, default:, fallback:)
            translation = create(locale: locale, key: key, value: default) if default.present?
            if fallback.present? && !translation.present?
              translation = create(locale: locale, key: key, value: fallback)
            end

            translation&.value
          end

          def reset_translations
            updated_at = maximum('updated_at')
            return unless @translations_updated_at != updated_at

            @translations_updated_at = updated_at
            @translations = nil
          end
        end
      end
    end
  end
end
