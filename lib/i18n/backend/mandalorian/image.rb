# frozen_string_literal: true

require 'active_record'

module I18n
  module Backend
    class Mandalorian
      class Image < ::ActiveRecord::Base
        self.table_name = 'mandalorian_images'

        has_one_attached :file

        validates :key, uniqueness: { scope: :locale }
        validates :key, presence: true

        def model_name
          ActiveModel::Name.new(self, nil, 'MandalorianImage')
        end

        def svg?
          file.filename.to_s.split('.')[-1] == 'svg'
        end
      end
    end
  end
end
