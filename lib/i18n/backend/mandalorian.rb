# frozen_string_literal: true

require 'i18n/backend/base'
require 'i18n/backend/mandalorian/seo'
require 'i18n/backend/mandalorian/translation'
require 'i18n/backend/mandalorian/image'
require 'active_support/core_ext/hash'

module I18n
  module Backend
    class Mandalorian
      # using I18n::HashRefinements

      module Implementation
        include Base

        protected

          def lookup(locale, key, scope = [], options = EMPTY_HASH)
            return fallback_lookup(locale, key, scope, options) if excluded_from_lookup?(key)

            translation = Translation.lookup(locale, key, scope, options)

            unless translation.present? || %i[test development].include?(Rails.env)
              translation = Translation.store(
                locale: locale,
                key: key,
                default: options[:translation],
                fallback: fallback_lookup(locale, key, scope, options)
              )
            end

            translation
          end

          def fallback_lookup(locale, key, scope = [], options = EMPTY_HASH)
            return unless I18n.backend.backends.size > 1

            fallback_backend = I18n.backend.backends[1]
            fallback_backend.send(:lookup, locale, key, scope, options)
          end

        private

          def excluded_from_lookup?(key)
            %i[activerecord attributes helpers views i18n errors mandalorian faker].each do |start|
              return true if key.to_s.start_with?(start.to_s)
            end

            false
          end
      end

      include Implementation
    end
  end
end
