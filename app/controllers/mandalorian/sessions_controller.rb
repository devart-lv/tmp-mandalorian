# frozen_string_literal: true

class Mandalorian::SessionsController < Mandalorian::ApplicationController
  def new
    session[:loggedin] = true
    redirect_to :home
  end

  def destroy
    session[:loggedin] = false
    reset_session
    redirect_to :home
  end
end
