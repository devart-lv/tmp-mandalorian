# frozen_string_literal: true

class Mandalorian::ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
end
