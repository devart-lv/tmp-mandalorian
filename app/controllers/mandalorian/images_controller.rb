# frozen_string_literal: true

class Mandalorian::ImagesController < Mandalorian::ApplicationController
  def edit
    @item = I18n::Backend::Mandalorian::Image.find params[:id]
  end

  def update
    @item = I18n::Backend::Mandalorian::Image.find params[:id]
    if @item.update(items_params)
      render action: :saved
    else
      render action: :new
    end
  end

  def lookup
    @item = I18n::Backend::Mandalorian::Image.where(locale: I18n.locale).find_by key: params[:key]
    render action: :edit
  end

  private

    def items_params
      params.require(:mandalorian_image).permit(:locale, :key, :file, :id)
    end
end
