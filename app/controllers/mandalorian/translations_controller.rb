# frozen_string_literal: true

class Mandalorian::TranslationsController < Mandalorian::ConfigurationController
  def index
    @items = I18n::Backend::Mandalorian::Translation.all.order(id: :asc)
  end

  def edit
    @item = I18n::Backend::Mandalorian::Translation.find params[:id]
  end

  def update
    @item = I18n::Backend::Mandalorian::Translation.find params[:id]
    if @item.update(items_params)
      respond_to do |format|
        format.html { redirect_to action: :index }
        format.js   { render action: :saved }
      end
    else
      render action: :new
    end
  end

  def lookup
    @item = I18n::Backend::Mandalorian::Translation.where(locale: I18n.locale).find_by key: params[:key]
    render action: :edit
  end

  def enable
    session[:edit_translations] = true
    redirect_back(fallback_location: '/')
  end

  def disable
    session[:edit_translations] = false
    redirect_back(fallback_location: '/')
  end

  private

    def items_params
      params.require(:mandalorian_translation).permit(:locale, :key, :value, :id)
    end
end
